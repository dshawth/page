# Home

`Hello World!` and welcome to my page.

Sharing some of my life as a 'readme' seemed much more palatable than sharing on social media so this is what you get.
I hope you find what you are looking for here.

## About

I am:

- driven by impact, inspired to make a difference
- passionate about cyber, fitness, ...

## Contact

If you need to get in touch with me, this simple challenge yields an alias:

> Merge the missing digits from the most famous party in the northeast with this URL.
> Use Alpha-Tango Papa-Mike as the symbol & domain, keep the TLD.
